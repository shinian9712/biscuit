
#define EXCP_ASYNC_SIG 0x80000000U

#define EXCP_INST_MISALIGN 0x1U // 指令未对齐
#define EXCP_ADDR_MISALIGN 0x2U // 访存未对齐
#define EXCP_UNDEF_INST 0x3U // 未定义指令
#define EXCP_ILLGAL_INST 0x4U // 特权级错误
#define EXCP_INST_PGFT 0x5U // 取指令PAGE FAULT
#define EXCP_LOAD_PGFT 0x6U // 读取 PAGE FAULT
#define EXCP_STOR_PGFT 0x7U // 存储 PAGE FAULT
#define EXCP_SYSCALL_U 0x8U // 系统调用
#define EXCP_ACCESS_MIS_X 0x9U // 缺X标志
#define EXCP_ACCESS_MIS_R 0xAU // 缺R标志
#define EXCP_ACCESS_MIS_W 0xBU // 缺W标志
#define EXCP_ACCESS_MIS_U 0xCU // 缺U标志
#define EXCP_ACCESS_INVADDR 0xDU // 无效地址
#define EXCP_TIMER 0x10U // 时钟中断
#define EXCP_DIVZERO 0x11U // 除0异常
#define EXCP_VMNIMPL 0xFFU // 虚拟机功能未实现

#define INTRP_CHRDEV_GETC 0x101 // 字符设备输入中断
#define INTRP_MFS_RET 0x102 // MagicFS设备完成
