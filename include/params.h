#define MAX_MOD_ITEM 16
#define MAX_MODS 16
#define MAX_FIELD 16
#define MAX_MEMSZ 0x8000000U // 128M

#define NR_RING_ITEM 1024

#define DEVPOLL_TIME 1
#define ITFPOOL_TIME 100000
#define INTRPOLL_TIME 10 // 异步中断中断poll time

#define PARAM_DEBUG 0

#define CPU_ORIPC 0x1000U