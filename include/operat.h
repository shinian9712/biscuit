enum rwmod {
    RW_R8 = 0x3, // 0011
    RW_R16 = 0x5, // 0101
    RW_R32 = 0x9, // 1001
    RW_W8 = 0x2, // 0010
    RW_W16 = 0x4, // 0100
    RW_W32 = 0x8, // 1000
    RW_X32 = 0x19 
};

#define IS_RMODE(rw) ((rw) % 2 == 1)
#define IS_WMODE(rw) ((rw) % 2 == 0)
#define IS_8BIT(rw) (((rw) & 0x2) != 0)
#define IS_16BIT(rw) (((rw) & 0x4) != 0)
#define IS_32BIT(rw) (((rw) & 0x8) != 0)
#define IS_XMODE(rw) (((rw) & 0x10) != 0)

struct req_stat{
    bool env_ch; // env是否发生变化?
    bool req_ack; // 有mod对此次req生效?
    bool req_fin; // 此次req是否已完成?
    modid_t hdmod [MAX_MOD_ITEM]; // 对此次请求起作用的mod
    count_t hdmod_n; 
    void * field_ch[MAX_FIELD]; // 指向变化字段的指针
    count_t field_n;
};

#define REQS_INIT {false,false,false,{},0,{},0}

struct cpu_csrrw_req {
    struct environ * env;
    void * ctx;
    csraddr_t addr;
    word send;
    word * recv;
    enum rwmod rw;
    struct req_stat * rstat;
};

struct dev_csrrw_req {
    struct environ * env;
    void * ctx;
    csraddr_t addr;
    word send;
    word * recv;
    enum rwmod rw;
    struct req_stat * rstat;
};

struct cpu_exhdlr_req {
    struct environ * env;
    void * ctx;
    word source;
    word cause;
    struct req_stat * rstat;
};

struct cpu_decoder_req {
    struct environ * env;
    void * ctx;
    word saved_pc;
    struct req_stat * rstat;
};

struct mem_virtrw_req {
    struct environ * env;
    void * ctx;
    memaddr_t addr; 
    word send;
    word * recv;
    enum rwmod rw;
    struct req_stat * rstat;
};
struct mem_phyrw_req {
    struct environ * env;
    void * ctx;
    memaddr_t addr; 
    word send;
    word * recv;
    enum rwmod rw;
    struct req_stat * rstat;
};

typedef status (*cpu_csrrw) (struct cpu_csrrw_req req);
typedef status (*cpu_exhdlr) (struct cpu_exhdlr_req req);
typedef status (*cpu_decoder) (struct cpu_decoder_req req);
typedef status (*mem_virtrw) (struct mem_virtrw_req req);
typedef status (*mem_phyrw) (struct mem_phyrw_req req);
typedef status (*dev_csrrw) (struct dev_csrrw_req req);
typedef status (*mod_fn) (struct environ * env, struct module * mod);
typedef status (*mod_exfn) (struct environ * env, struct module * mod, uint64 arg);

enum cpu_ctl_arg {
    CPU_CTL_RESET = 0x0,
    CPU_CTL_START = 0x1,
    CPU_CTL_STOP = 0x2,
};

enum env_ctl_arg {
    ENV_CTL_RESET = 0x0,
    ENV_CTL_START = 0x1,
    ENV_CTL_STOP = 0x2,
    ENV_CTL_PAUSE = 0x3,
    ENV_CTL_RESUME = 0x4
};

enum dev_ctl_arg {
    DEV_CTL_START = 0x1,
    DEV_CTL_STOP = 0x2,
    DEV_CTL_PAUSE = 0x3,
    DEV_CTL_RESUME = 0x4,
};

enum mod_ctl_arg {
    MOD_CTL_START = 0x1,
    MOD_CTL_STOP = 0x2,
    MOD_CTL_PAUSE = 0x3,
    MOD_CTL_RESUME = 0x4,
    MOD_CTL_CTRL = 0x5
};