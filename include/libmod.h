#include "types.h"
#include "params.h"
#include "operat.h"
#include "module.h"
#include "virtware.h"
#include "types.h"
#ifndef DBG_LOG
#define DBG_LOG(domain,fmt,arg...) {if(PARAM_DEBUG){printf("DEBUG:%s: ", #domain);printf(fmt,##arg);printf("\n");fflush(stdout);}}
#endif

#ifndef MEM_BARRIER
#define MEM_BARRIER {__asm__ __volatile__("": ::"memory");}
#endif

status raise_excep_sync (struct environ* env, word cause);
status raise_excep_async (struct environ* env, word cause);

status memrw_phy (struct environ* env, memaddr_t addr, word send, word * recv, enum rwmod rw);
status memrw_virt (struct environ* env, memaddr_t addr, word send, word * recv, enum rwmod rw);

status csrrw_lazy (struct environ* env, csraddr_t addr, word send, word * recv, enum rwmod rw, struct req_stat * async_stat);
status csrrw_sync  (struct environ* env, csraddr_t addr, word send, word * recv, enum rwmod rw);

status module_add_item (struct module * mod, enum module_item_type type, void * func);
status env_ctrl_mod (struct environ* env, modid_t modid, enum mod_ctl_arg arg, uint64 exarg);
status env_add_mod (struct environ * env, struct module * mod);
status env_remove_mod (struct environ * env, modid_t modid);