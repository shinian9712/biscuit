/* TODO: 以bit标定错误 */

#define ERR_AGAIN 0x1 // invoke again
#define ERR_MODFN 0x2 // error occurred in mod function
#define ERR_MNOINST 0x4 // mod not install
#define ERR_MAGIC 0x8 // invalid magic number
#define ERR_NOIMPL 0x10 // not implement
#define ERR_FULLRING 0x20 // full ring
#define ERR_EMPTYRING 0x40 // empty ring
#define ERR_NRITEM 0x80 // max mod items
#define ERR_NRMOD 0x100 // max mods
#define ERR_CONTROL 0x200 // unable to control (error occurred while applying request)
#define ERR_THREAD 0x400 // thread error
#define ERR_NOMEM 0x800  // insufficient memory
#define ERR_MADDR 0x1000 // invalid memory address
#define ERR_MFLAG 0x2000 // invalid memory flag
#define ERR_NOACK 0x4000 // requset not acknowledged
#define ERR_STATUS 0x8000 // wrong status
#define ERR_CTXSTAT 0x10000 // wrong context status
#define ERR_INVARG 0x20000 // invalid argument
#define ERR_INVELF 0x40000 // invalid elf file
#define ERR_MALIGN 0x80000 // address misaligned
#define ERR_SYSTEM 0x100000 // system error
