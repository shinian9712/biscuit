typedef long long int64;
typedef unsigned long long uint64;
typedef int int32;
typedef unsigned int uint32;
typedef short int16;
typedef unsigned short uint16;
typedef char int8;
typedef unsigned char uint8;
typedef uint8 bool;
typedef uint32 count_t;
typedef int32 word;
typedef int64 status;
typedef uint64 modid_t;
typedef uint16 csraddr_t;
typedef uint32 memaddr_t;
typedef uint64 magic_t;

#define true 1
#define false 0

#define MODID_INVAILD 0
#define BISCUIT_MAGIC 0x74697563736962U


#define CAUSE_BKI_CONS 1
#define CAUSE_BKI_BLK 2
#define CAUSE_BKI_NIC 3

#define STAT_SUCCESS 0

struct environ;
struct rv32cpu;
struct memory;
struct devctl;
struct module;
struct module_item;