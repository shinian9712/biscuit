CORE=core
MOD=module

CC = gcc
CFLAGS = -Werror -Wextra -Wall

core_objs = \
	$(CORE)/cpu.o \
	$(CORE)/devctl.o \
	$(CORE)/environ.o \
	$(CORE)/kloader.o \
	$(CORE)/memory.o \
	$(CORE)/module.o \
	$(CORE)/libmod.o

mod_objs = \
	$(MOD)/mod_bv32vm.o \
	$(MOD)/mod_chrdev.o \
	$(MOD)/mod_rv32g.o \
	$(MOD)/mod_magicfs.o

vm : $(core_objs) $(mod_objs) init.o
	$(CC) $(CFLAGS) -pthread -o vm $(core_objs) $(mod_objs) init.o

init.o : init.c
	$(CC) $(CFLAGS) -c init.c -o init.o

clean:
	rm $(core_objs) $(mod_objs) vm init.o