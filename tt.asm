.data
.space 1024000
.bss
.space 10240
.text
lui x31, 1048575 #-1499824128
auipc x1, 1031111 #1162973184
srli x0, x12, 31
addi x31, x0, -1
slti x30, x1, 0
sltiu x29, x1, 2047
xori x28, x1, -2048
andi x27, x2, -3
slli x26, x3, 31
srli x25, x4, 2
srai x24, x5, 2
add x23, x6, x1
sub x22, x7, x2
sll x21, x8, x3
slt x20, x9, x4
sltu x19, x10, x5
xor x18, x11, x6
srl x17, x12, x7
sra x16, x13, x8
or x15, x14, x9
and x14, x15, x10
lb x31, -1(x1)
lh x31, 2047(x2)
lw x30, -1(x3)
lbu x29, -2048(x4)
lhu x28, 0(x5)
sb x27, -1(x6) #0101 1101 1010b
sh x26, 1234(x7)
sw x25, -1424(x8)
l0:
l1:
beq x21, x2, l0
add x1, x0, x0
