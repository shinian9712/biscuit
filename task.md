- RV32G ISA
    - [X] RV32I
    - [X] RV32M
    - [X] RV32Zicsr
- HTIF
    - [ ] open
    - [ ] read
    - [ ] write
    - [ ] close
- OS Kernel
    - [ ] fork
    - [ ] execve
    - [ ] Pipe
    - [ ] open (HTIF)
    - [ ] read (HTIF)
    - [ ] write (HTIF)
    - [ ] close (HTIF)