#include <stdio.h>

void
putd (int d) {
    char buffer[20];
    unsigned int u;
    if (d < 0) {
        u = d;
        u = ~u;
        u += 1;
        printf ("%c",'-');
    } else {
        u = d;
    }
    int i,j,tmp;
    for (i = 0; i < 20; i++) {
        buffer[i] = 0;
    }
    if (d == 0) {
        buffer[0] = '0';
        goto print;
    }
    i = 0;
    while (u) {
        buffer[i] = u % 10 + '0';
        u /= 10;
        i++;
    }
    j = i-1;
    for (i = 0; i <= j/2; i++) {
        tmp = buffer[i];
        buffer[i] = buffer[j-i];
        buffer[j-i] = tmp;
    }
    print:
    printf ("%s", buffer);
}

int
main () {
    unsigned int t;
    t = 0x80000000U;
    for (unsigned int i = t; i > 0x12345U; i -= 0x12345U) {
        if ((unsigned int) i <= 0x12345U)
            putd (i);
        
    }
    //printf ("%s",buffer);
}