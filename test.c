#include <pthread.h>
#include <stdio.h>
#define NR_RING_ITEM 1000
// pthread_mutex_t mtx;

#define ring_full(ring) ((ring.tail+1) % NR_RING_ITEM == ring.head)
#define ring_empty(ring) (ring.tail == ring.head)
// 约定 head==tail为空, tail+1 == head为满
// 操作成功返回1,否则返回0
#define ring_pushhd(ring, item) (ring_full(ring) ? 0 :\
((ring.head?ring.data[--(ring.head)]=item:(ring.data[ring.head=NR_RING_ITEM-1]=item)), 1))

#define ring_pophd(ring, item) (ring_empty(ring) ? 0 :\
(item=ring.data[ring.head], (ring.head==NR_RING_ITEM-1?ring.head=0:ring.head++), 1))

#define ring_pushtl(ring, item) (ring_full(ring) ? 0 :\
(ring.data[ring.tail]=item,ring.tail==NR_RING_ITEM-1?ring.tail=0:ring.tail++, 1))

#define ring_poptl(ring, item) (ring_empty(ring) ? 0 :\
(ring.tail==0?ring.tail=NR_RING_ITEM-1:(ring.tail--, item=ring.data[ring.tail]), 1))

int
main () {
    struct {
        unsigned int head;
        unsigned int tail;
        int data[NR_RING_ITEM];
    } test_ring = {
        .head = 0,
        .tail = 0
    };
    int op_suc;
    int i, o;
    for (i = 0; i < 1100; i++) {
        ring_pushtl (test_ring, i);
    }
    for (i = 0; i < 100; i++) {
        if (ring_pophd (test_ring, o))
            printf ("%d ", o);
    }
    printf ("\n");
    for (i = 0; i < 100; i++) {
        if (ring_poptl (test_ring, o))
            printf ("%d ", o);
    }
    for (i = 0; i < 100; i++) {
        if (ring_pophd (test_ring, o))
            printf ("%d ", o);
    }
    printf ("\n");
    for (i = 0; i < 100; i++) {
        if (ring_poptl (test_ring, o))
            printf ("%d ", o);
    }
    for (i = 0; i < 100; i++) {
        if (ring_pophd (test_ring, o))
            printf ("%d ", o);
    }
    printf ("\n");
    for (i = 0; i < 100; i++) {
        if (ring_poptl (test_ring, o))
            printf ("%d ", o);
    }
}