#define CSR_MFSCTL 0xBD5
#define CSR_MFSFUNC 0xBD6
#define CSR_MFSARG0 0xBD7
#define CSR_MFSARG1 0xBD8
#define CSR_MFSARG2 0xBD9
#define CSR_MFSARG3 0xBDA
#define CSR_MFSRETV 0xBDB
#define CSR_MFSCRATCH 0xBDC

#define MAX_MFSFILE 256
#define MAX_NAMESZ 16

#define MFS_FETCH 1
#define MFS_RM 2
#define MFS_READ 3
#define MFS_WRITE 4

#define MFSCTL_INVOKE 0x1
#define MFSCTL_DEVREADY 0x2

#define MFS_HOME "magicfs"