#define ZEXT(x) ((unsigned int)(x))

#define PGSZ 4096U
// #define MEM_SIZE (MEM_PG_NUM * PG_SIZE)
// #define RV32_ALIGNED __attribute__((aligned(4)))

#define L1_PTE_INDEX(va) (ZEXT(va & 0xFFC00000U) >> 22)
#define L2_PTE_INDEX(va) (ZEXT(va & 0x3FF000U) >> 12)
#define PADDR_OFFSET(va) (ZEXT(va & 0xFFFU))
#define PG4K_MASK 0xFFFU
#define FLAG_MASK 0xFFF
#define PTE_U 0x10U
#define PTE_X 0x8U
#define PTE_W 0x4U
#define PTE_R 0x2U
#define PTE_V 0x1U

#define CSR_MATP 0xBC0