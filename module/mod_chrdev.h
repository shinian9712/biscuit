#define INPOLL_TIME -1

#define CSR_CONCTL 0xBD2
#define CSR_CONRECV 0xBD3
#define CSR_CONSEND 0xBD4

#define DRV_READY(csr) (csr & 0x10)
#define DEV_READY(csr) (csr & 0x8)

#define IO_READY(csr) (csr & 0x4)
#define RECV_READY(csr) (csr & 0x2)
#define SEND_READY(csr) (csr & 0x1)

#define SET_RECV_READY(csr) (csr |= 0x2)
#define CLR_SEND_READY(csr) (csr &= ~0x1)