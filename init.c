#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include "include/params.h"
#include "include/types.h"
#include "include/operat.h"
#include "include/virtware.h"
#include "include/module.h"
#include "include/libcore.h"

struct environ default_env;
struct modlist default_modlist;

void
stop_sighdlr (int sig) {
    (void) sig;
    env_ctrl (&default_env, ENV_CTL_STOP);
}

int
main (int argc, char ** argv) {
    if (argc != 2) {
        printf ("usage: %s kernel\n", argv[0]);
        return 0;
    } else {
        env_init (&default_env, MAX_MEMSZ);
        if (kl_load_kernel (argv[1], default_env.mem.data, MAX_MEMSZ))
            return 0;
    }
    modlist_init (&default_modlist);
    modlist_install_all (&default_env, &default_modlist);
    signal (SIGTSTP, stop_sighdlr);
    env_ctrl (&default_env, ENV_CTL_START);
    printf ("*** VM running ***\n");
    fflush (stdout);
    for (;;) {
        usleep (ITFPOOL_TIME);
        if (default_env.stat == ENV_STOPPED) {
            printf ("*** VM stopped ***\n");
            break;
        }
    }
}
