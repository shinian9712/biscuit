# 设备说明书
Biscuit-VM 目前支持四种设备,分别调试设备,时钟设备,Biscuit-VirtMem和Biscuit-Console. Biscuit-HTIF目前未实现. 目前支持RV32IM和Zicsr指令集.
## 调试设备 
目前调试设备支持打印指定寄存器中的数字及其所指向位置的字符串.  
输出信息如下所示:  
- 输出指定数字 TESTING NUM :%d 0x%X
- 输出指定字符串 TESTING STR :%s @ 0x%X

## 时钟设备
目前时钟设备仅支持使用指令计时,不能反映真实时间.时钟设备维护两个寄存器,分别是MTIME寄存器和MTIMECMP寄存器.
- 每执行一条指令,MTIME寄存器加1.
- 当时钟中断使能为真且MTIME的值大于等于MTIMECMP时,发起时钟中断.
- 当时钟中断发起后,时钟中断使能被赋值为假.
- 当写入MTIMECMP寄存器时,时钟中断使能被赋值为真.

## 分页机构 Biscuit-VirtMem
### 分页方案
Biscuit-VirtMem分页机构目前支持BV32分页方案(非标准).分页机构维护一个寄存器MATP,置0关闭分页. 方案规定如下:  
L1PT(一级页表) = *MATP(页表寄存器)  
VA(虚拟地址):    | VPN.0 [31:22] | VPN.1 [21:12] |   OFFSET [11:0]  |  
PTE(页表项):     |          PPN [31:12]          |   FLAGS  [11:0]  |  
L2PT(二级页表):  |  L1PT[VA.VPN.0].PPN [31:12]   |   000000000000   |  
PA(物理地址):    |  L2PT[VA.VPN.1].PPN [31:12]   | VA.OFFSET [11:0] |  
方案使用二级页表,每个页表4KB,页表项4B. 每个页表有1024个页表项.  
保护模式下若出现访存错误,则发起页错误中断,同时将MTVAL寄存器填入发生错误的地址  

## 字符设备 Biscuit-Console 
| NAME/Bits | RESERVE[31:5] |      [4]     |   [3]     |    [2]   |     [1]     |       [0]       |
|-----------|---------------|--------------|-----------|----------|-------------|-----------------|
| Cons_CTL  |               | Driver_Ready | Dev_Ready | IO_Ready | RECV_Ready  |    SEND_Ready   |
| Cons_RECV |                                         DATA                                        |
| Cons_SEND |                                         DATA                                        |

- RECV_Ready: 输入就绪指示位,设备置1告知OS Cons_RECV有效,OS需读数据。OS置0告知设备可读数据
- SEND_Ready: 输出就绪指示位,OS置1告知设备 Cons_SEND有效,设备需写数据。设备置0告知OS可写数据
- 输入数据: 键盘输入 -> 设备等待RECV_Ready=0 -> 设备写Cons_RECV=ch -> 设备写RECV_Ready=1 -> 发出中断 -> OS处理中断,读Cons_RECV -> OS写RECV_Ready=0
- 输出数据: OS等待SEND_Ready=0 -> OS写入Cons_SEND=ch -> OS写入SEND_Ready=1 -> 设备读SEND_Ready=1 -> 设备读Cons_SEND=ch -> 打印ch -> 设备写SEND_Ready=0
- Console Send只有同步模式,Receive只有中断模式

## 虚拟文件系统设备 Biscuit-MagicFS
| NAME/Bits |                     RESERVE[31:2]                   |     [1]     |       [0]       |
|-----------|-----------------------------------------------------|-------------|-----------------|
|  MFS_CTL  |                                                     |  DEV_REDAY  |      INVOKE     |
|  MFS_FUNC |                                        FUNCT                                        |
|  MFS_ARG0 |                                         ARG0                                        |
|  MFS_ARG1 |                                         ARG1                                        |
|  MFS_ARG2 |                                         ARG2                                        |
|  MFS_ARG3 |                                         ARG3                                        |
|  MFS_RETV |                                        RETVAL                                       |
|  MFS_SCRATCH |                                    SCRATCH                                       |
MagicFS虚拟文件系统设备支持文件按名读写,任意位置任意长度读写,返回可用文件列表.
```
char lsbuf [256][16];
/* 参数 path:文件路径, off:文件偏移地址, buf:目标缓冲区, sz:需读/写字节数 */
int fetch (&lsbuf); // 将文件名填充至数组,并返回文件个数 FUNCT=1
int rm (char * path); // 删除文件 FUNCT=2
int read (char * path, unsigned int off, void * buf, unsigned int sz); // FUNCT=3
int write (char * path, unsigned int off ,void * buf, unsigned int sz); // FUNCT=4
```
- DEV_REDAY: 设备就绪指示位, 设备置1告知OS设备有效
- INVOKE: /调用开关 OS置1告知MagicFS执行函数, MagicFS置0告知OS可执行函数.
- FUNCT: 调用函数ID, 0:fetch, 1:read, 2:write
- ARGX: X={0,1,2,3} 函数参数
- RETVAL: 返回值
- 执行函数: OS等待INVOKE=0 -> OS填入参数 -> OS将INVOKE置1 -> 等待中断 -> 处理中断


## 控制寄存器一览表
- 默认控制寄存器
    - CSR_MSTATUS 0x300 (部分功能未实现)
    - CSR_MIE 0x304 (未实现)
    - CSR_MTVEC 0x305
    - CSR_MSCRATCH 0x340
    - CSR_MEPC 0x341
    - CSR_MCAUSE 0x342
    - CSR_MTVAL 0x343
    - CSR_MIP 0x344 (未实现)
    - CSR_HALT 0xC00
- 调试设备
    - CSR_TESTNUM 0xF20
    - CSR_TESTSTR 0xF21
- 时钟设备
    - CSR_MTIME 0xF15
    - CSR_MTIMECMP 0xF16
- Biscuit-VirtMem
    - CSR_MATP 0xBC0
- Biscuit-Console 
    - CSR_CONCTL 0xBD2
    - CSR_CONRECV 0xBD3
    - CSR_CONSEND 0xBD4
- Biscuit-MagicFS
    - CSR_MFSCTL 0xBD5
    - CSR_MFSFUNC 0xBD6
    - CSR_MFSARG0 0xBD7
    - CSR_MFSARG1 0xBD8
    - CSR_MFSARG2 0xBD9
    - CSR_MFSARG3 0xBDA
    - CSR_MFSRETV 0xBDB
    - CSR_MFSCRATCH 0xBDC

## 内部异常一览表
- EXCP_INST_MISALIGN 0x1U // 指令未对齐
- EXCP_ADDR_MISALIGN 0x2U // 访存未对齐
- EXCP_UNDEF_INST 0x3U // 未定义指令
- EXCP_ILLGAL_INST 0x4U // 特权级错误
- EXCP_INST_PGFT 0x5U // 取指令PAGE FAULT
- EXCP_LOAD_PGFT 0x6U // 读取 PAGE FAULT
- EXCP_STOR_PGFT 0x7U // 存储 PAGE FAULT
- EXCP_SYSCALL_U 0x8U // 系统调用
- EXCP_ACCESS_MIS_X 0x9U // 缺X标志
- EXCP_ACCESS_MIS_R 0xAU // 缺R标志
- EXCP_ACCESS_MIS_W 0xBU // 缺W标志
- EXCP_ACCESS_MIS_U 0xCU // 缺U标志
- EXCP_ACCESS_INVADDR 0xDU // 无效地址
- EXCP_TIMER 0x10U // 时钟中断
- EXCP_DIVZERO 0x11U // 除0异常
- EXCP_VMNIMPL 0xFFU // 虚拟机功能未实现

## 外部中断一览表
- INTRP_CHRDEV_GETC 0x101 // 字符设备输入中断
- INTRP_MFS_RET 0x102 // MagicFS设备函数执行完成