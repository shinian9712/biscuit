#include <pthread.h>
typedef struct {
    pthread_mutex_t mtx;
    pthread_mutex_t start;
    bool idle;
    bool devready;
    bool drivready;
    bool ioready;
    bool recvready;
    bool sendready;
    uint32 send;
    uint32 recv;
} cons_t;

typedef struct {
    pthread_mutex_t mtx;
    pthread_mutex_t start;
    bool idle;
    bool devready;
    bool drivready;
    bool ioready;
    uint32* memaddr;
    uint64 fileaddr;
    uint32 blks;
} blk_t;

typedef struct {
    pthread_mutex_t mtx;
    pthread_mutex_t start;
    bool idle;
    bool cons_ie;
    bool blk_ie;
    bool timer_ie;
    bool nic_ie;
    uint32 info;
    uint32 src;
} ic_t;

typedef struct {
    pthread_mutex_t mtx;
    pthread_mutex_t start;
    bool idle;
    uint32 tmspan;
} timer_t;