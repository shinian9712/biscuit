/* stat: |CPU: 31~24|MEM: 23~16|IO: 15~8|OTH: 7~0| */

#define E_CPU_BAD_OP 0x80000000U
#define E_CPU_PC_NOT_ALIGNED 0x40000000U
#define E_CPU_INTERNAL_ERR 0x20000000U

#define E_MEM_INV_ADDR 0x800000U
#define E_MEM_BV32_PGT_NOT_ALIGNED 0x400000U
#define E_MEM_BV32_PTE_MISSING_X 0x200000U
#define E_MEM_BV32_PTE_MISSING_W 0x100000U
#define E_MEM_BV32_PTE_MISSING_R 0x80000U
#define E_MEM_BV32_PTE_MISSING_V 0x40000U
#define E_MEM_INV_MODE 0x20000U

#define E_ELF_NEED_MORE_MEM  0x80U
#define E_ELF_FILE_ERR 0x40U


////////

#define E_CPU_BAD_OP_MSG "Bad instruction encountered."
#define E_CPU_PC_NOT_ALIGNED_MSG "PC not aligned."
#define E_CPU_INTERNAL_ERR_MSG "Virtural machine internal error."

#define E_MEM_INV_ADDR_MSG "Invalid memory address."
#define E_MEM_BV32_PGT_NOT_ALIGNED_MSG "BV32 page table not aligned."
#define E_MEM_BV32_PTE_MISSING_X_MSG "BV32 page table entry missing flag X"
#define E_MEM_BV32_PTE_MISSING_W_MSG "BV32 page table entry missing flag W"
#define E_MEM_BV32_PTE_MISSING_R_MSG "BV32 page table entry missing flag R"
#define E_MEM_BV32_PTE_MISSING_V_MSG "BV32 page table entry missing flag V"
#define E_MEM_INV_MODE_MSG "Invalid memory addressing mode."

#define E_ELF_NEED_MORE_MEM _MSG "Virtual machine need more memory to load ELF file."
#define E_ELF_FILE_ERR_MSG "Program encountered a file error."