#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "types.h"
#include "defs.h"

uint32 insts[1024000];

int
main (int argc, char ** argv) {
    // -86
    (void) argc;
    (void) argv;
    int fd;
    int n, i;
    fd = open ("./a.bin", O_RDONLY);
    if (fd == -1) return 1;
    n = read (fd, insts, sizeof (insts));
    for (i = 0; i < n / 4; i++) {
        rv32i_tell (insts[i]);
    }
    // printf ("%x",a);
    return 0;
}