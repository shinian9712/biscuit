typedef long long int64;
typedef unsigned long long uint64;
typedef int int32;
typedef unsigned int uint32;
typedef short int16;
typedef unsigned short uint16;
typedef char byte;
typedef unsigned char ubyte;
typedef byte bool;

#define SEXT (int32)
#define ZEXT (uint32)
#define true 1
#define false 0
