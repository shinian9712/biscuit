#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "defs.h"
#include "cpu.h"
#include "rv32.h"
#include "err.h"

int
rv32m_tell (int inst) {
    DEF_R_INST (MUL, 0x1, 0x0, 0x33, {});
    DEF_R_INST (MULH, 0x1, 0x1, 0x33, {});
    DEF_R_INST (MULHSU, 0x1, 0x2, 0x33, {});
    DEF_R_INST (MULHU, 0x1, 0x3, 0x33, {});
    DEF_R_INST (DIV, 0x1, 0x4, 0x33, {});
    DEF_R_INST (DIVU, 0x1, 0x5, 0x33, {});
    DEF_R_INST (REM, 0x1, 0x6, 0x33, {});
    DEF_R_INST (REMU, 0x1, 0x7, 0x33, {});

    printf ("Not a rv32m inst. Forwarding to next ISA ...\n");
    return -1;
    executed:
    return 0;
}

int
rv32m_exec () {

}