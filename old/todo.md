# Biscuit

## Biscuit-CPU
### RV32I
#### R-Type
- [ ] ADD
- [ ] SUB
- [ ] AND
- [ ] OR
- [ ] XOR
- [ ] SLL
- [ ] SRL
- [ ] SRA
- [ ] SLT
- [ ] SLTU
#### I-Type
- [ ] ADDI
- [ ] ANDI
- [ ] ORI
- [ ] XORI
- [ ] SLLI
- [ ] SRLI
- [ ] SRAI
- [ ] SLTI
- [ ] SLTIU
- [ ] LB
- [ ] LH
- [ ] LW
- [ ] LBU
- [ ] LHU
- [ ] JALR
#### S-Type
- [ ] SB
- [ ] SH
- [ ] SW
#### B-Type
- [ ] BEQ
- [ ] BNE
- [ ] BLT
- [ ] BGE
- [ ] BLTU
- [ ] BGEU
#### U-Type
- [ ] LUI
- [ ] AUIPC
#### J-Type
- [ ] JAL
### RV32Zicsr
- [ ] FENCE
- [ ] FENCE.I
- [ ] ECALL
- [ ] EBREAK
- [ ] CSRRW
- [ ] CSRRS
- [ ] CSRRC
- [ ] CSRRWI
- [ ] CSRRSI
- [ ] CSRRCI
- [ ] SFENCE.VMA

## Biscuit-Console 
| NAME/Bits | RESERVE[31:5] |      [4]     |   [3]     |    [2]   |     [1]     |       [0]       |
|-----------|---------------|--------------|-----------|----------|-------------|-----------------|
| Cons_CSR  |               | Driver_Ready | Dev_Ready | IO_Ready | RECV_Ready  |    SEND_Ready   |
| Cons_RECV |                                         DATA                                        |
| Cons_SEND |                                         DATA                                        |

- RECV_Ready: 输入就绪指示位,设备置1告知OS Cons_RECV有效,OS需读数据。OS置0告知设备可读数据
- SEND_Ready: 输出就绪指示位,OS置1告知设备 Cons_SEND有效,设备需写数据。设备置0告知OS可写数据
- 输入数据: 键盘输入 -> 设备等待RECV_Ready=0 -> 设备写Cons_RECV=ch -> 设备写RECV_Ready=1 -> 发出中断 -> OS处理中断,读Cons_RECV -> OS写RECV_Ready=0
- 输出数据: OS等待SEND_Ready=0 -> OS写入Cons_SEND=ch -> OS写入SEND_Ready=1 -> 设备读SEND_Ready=1 -> 设备读Cons_SEND=ch -> 打印ch -> 设备写SEND_Ready=0
- Console Send只有同步模式,Receive只有中断模式


## Biscuit-InterruptController
|   NAME   | [31:16] | [15:8] |  [7] |  [6]  | [5]  |  [4]  |   [3]  |  [2]   |   [1]   |    [0]   |
|----------|---------|--------|------|-------|------|-------|--------|--------|---------|----------|
| INTR_CSR |         |        |      |       |      |       | Blk_IE | Nic_IE | Cons_IE | Timer_IE  |
| INTR_SRC |   INFO  |        |      |       |      |       |Blk_INTR|Nic_INTR|Cons_INTR|Timer_INTR|

INFO: 
- CONS_INTR:[31] 0:SUCCESS, 1:FAILED; [30:16] 1:RECV
- NIC_INTR: [31] 0:SUCCESS, 1:FAILED; [30:16] 0:SEND  1:RECV
- BLK_INTR: [31] 0:SUCCESS, 1:FAILED; [30:16] 0:STORE 1:LOAD
- Timer_INTR: undefined

## Biscuit-Timer
| NAME/Bits | RESERVE[31:5] |      [4]     |   [3]     |    [2]   |     [1]     |       [0]       |
|-----------|---------------|--------------|-----------|----------|-------------|-----------------|
| Timer_CSR |               | Driver_Ready | Dev_Ready | IO_Ready |Timer_SPAN_MODE| Timer_ENABLE |
|Timer_Span |                                         Time                                        |

- Timer_ENALBE:置1启动Timer,置0关闭Timer
- Timer_SPAN_MODE: 置1每次中断时间间隔为Timer_Span,0默认10ms
- Timer_Span:时间间隔,单位微秒 

## Biscuit-Block
| NAME/Bits | BLOCK_N[31:24] |RESERVE|      [4]     |   [3]     |    [2]   |     [1]     |       [0]       |
|    ---    |     ---       | ---   |      ---     |    ---    |    ---   |     ---     |       ---       |
|  Blk_SR   |               |       | Driver_Ready | Dev_Ready | IO_Ready |   I/O Mode  |     IO_Start    |
|  Blk_MEM_ADDR |           |                               ADDR                                  |
|  Blk_DEV_ADDR |           |                               ADDR                                  |

- I/O Mode: 指定此次IO输入/输出模式 0:输出 1:输入
- IO_Start: OS写入1启动I/O
- Blk_MEM_ADDR: 内存地址,512B对齐
- Blk_DEV_ADDR: 设备地址
- BLOCK_N: 数据大小,单位Block(512B)
- 从设备读取数据: OS等待IO_Start=0 -> Blk_MEM_ADDR 填入内存地址, Blk_DEV_ADDR 填入设备地址 -> BLOCK_N=要读取块数,I/O mode=1,IO_Start=1 -> 设备启动IO -> IO完成后向OS发出中断
- 向设备写入数据: OS等待IO_Start=0 -> Blk_MEM_ADDR 填入内存地址, Blk_DEV_ADDR 填入设备地址 -> BLOCK_N=要写入块数,I/O mode=0,IO_Start=1 -> 设备启动IO -> IO完成后向OS发出中断



## Biscuit-Nic
- Nic_CSR
- Nic_In_Addr
- Nic_Out_Addr