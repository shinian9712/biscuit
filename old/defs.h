/* rv32i.c */
int rv32i_tell (int inst);
int rv32i_exec ();

/* rv32m.c */
int rv32m_tell (int inst);

/* rv32fd.c */
int rv32fd_tell (int inst);


/* cpu.c */
int cpu_intr (uint32 src);

/* mem.c */
int mem_phy_read (uint32 addr, int32 * buf, int mode);
int mem_phy_write (uint32 addr, int32 value, int mode);
int bv32_mem_virt_read (uint32 addr, uint32 pgt_addr, int32 * buf, int mode);
int bv32_mem_virt_write (uint32 addr, uint32 pgt_addr, int32 value, int mode);

/* elf_loader.c */
int elf32_load_kernel (const char* kernel_path, byte * mem, uint32 memsz);

/* device.c */
int consinit ();
int conscsrrw ();
int icinit();
int iccsrrw ();
int blkinit ();
int blkcsrrw ();

/* misc */



#define PRINT_ERROR(where, stat) {\
    fprintf (stderr, "%s", #where );\
}