#include <stdio.h>
#include <stdlib.h>

typedef union {
    char data[8];
    double d;
} double_t;

int
main () {
    double_t a, b;
    int i;
    a.d = 12345.67890123;
    for (i = 6; i < 8; i++) {
        b.data[i] = a.data[i];
        printf ("%X ", (char)a.data[i]);
    }
    printf ("\n");
    printf ("%lf\n",b.d);
}