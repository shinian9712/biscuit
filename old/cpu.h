#define MAX_CSR_NUM 0x1000U
#define MAX_XR_NUM x_num

typedef enum {
    x0 = 0,
    x1, x2, x3, x4, x5, x6, x7, x8, x9, x10,
    x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,
    x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,
    x31,x_num
} rv32_reg_t;

typedef enum {
    csr_mstatus = 0x300,
    csr_mie = 0x304,
    csr_mtvec = 0x305,
    csr_mscratch = 0x340,
    csr_mepc = 0x341,
    csr_mcause = 0x342,
    csr_mtval = 0x343,
    csr_mip = 0x344,
    csr_matp = 0xBC0, // pgt_csr
    csr_ics = 0xBD0, // intr_csr
    csr_isrc = 0xBD1, // intr_src
    csr_consc = 0xBD2, // cons_csr
    csr_consr = 0xBD3, // cons_recv
    csr_conss = 0xBD4, // cons_send
    csr_tmc = 0xBD5, // timer_csr
    csr_tms = 0xBD6, // time_span
    csr_blkcs = 0xBD7, // block_csr
    csr_blkma = 0xBD8, // block_mem_addr
    csr_blkda = 0xBD9 // block_dev_addr
} rv32_csr_t;

typedef enum {
    CPU_MODE_M = 0x0,
    CPU_MODE_U = 0x3
} rv32_cpu_mode_t;

typedef struct {
    int32 regs[x_num]; // 寄存器
    int32 csrs[MAX_CSR_NUM];
    pthread_mutex_t csr_mtx;
    int32 ir; // 指令寄存器
    uint32 pc; 
    rv32_cpu_mode_t mode;
    int32 * mem; // 内存指针
    uint32 mem_size; // 内存大小,单位Byte
    bool intr_enable;
    struct {
        bool intrset;
        bool isintr;
        bool isexp;
        uint32 src;
    } intr_info;
    pthread_mutex_t intr_mtx;
    struct {

    } mtval;
    struct {

    } mstatus;
    struct {

    } mcause;
    struct {

    } mie;
    struct {

    } mip;
    int32 mscratch;

} rv32_cpu_t;

extern rv32_cpu_t default_cpu;

#define CPU_INTR_LOCK(mycpu) { \
    pthread_mutex_lock(&mycpu->intr_mtx); \
}

#define CPU_INTR_UNLOCK(mycpu) { \
    pthread_mutex_unlock(&mycpu->intr_mtx); \
}