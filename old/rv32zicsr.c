#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "defs.h"
#include "cpu.h"
#include "rv32.h"
#include "err.h"

int
rv32zicsr_tell (int inst) {
    DEF_I_INST (CSRRW, 0x1, 0x73, {});
    DEF_I_INST (CSRRS, 0x2, 0x73, {});
    DEF_I_INST (CSRRC, 0x3, 0x73, {});
    DEF_I_INST (CSRRWI, 0x5, 0x73, {});
    DEF_I_INST (CSRRSI, 0x6, 0x73, {});
    DEF_I_INST (CSRRCI, 0x7, 0x73, {});
    DEF_UNIQUE_INST (ECALL, 0x73, {});
    DEF_UNIQUE_INST (EBREAK, 0x100073, {});
    DEF_UNIQUE_INST (URET, 0x200073, {});
    DEF_UNIQUE_INST (SRET, 0x10200073, {});
    DEF_UNIQUE_INST (MRET, 0x30200073, {});
    DEF_UNIQUE_INST (WFI, 0x10500073, {});
    DEF_R_INST (SFENCE_VMA, 0x9, 0x0, 0x73, {});
    DEF_UNIQUE_INST (FENCE_I, 0x100F, {});
    printf ("Not a rv32Zicsr inst. Forwarding to next ISA.\n");
    return -1;
    executed:
    return 0;
}