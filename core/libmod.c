#include <pthread.h>
#include "include/types.h"
#include "include/params.h"
#include "include/operat.h"
#include "include/params.h"
#include "include/virtware.h"
#include "include/module.h"
#include "include/vmerr.h"
#include "include/libcore.h"

status
memrw_phy (struct environ* env, memaddr_t addr, word send, word * recv, enum rwmod rw) {
    return mem_rw_phy (env, addr, send, recv, rw);
}

status
memrw_virt (struct environ* env, memaddr_t addr, word send, word * recv, enum rwmod rw) {
    return mem_rw_virt (env, addr, send, recv, rw);
}

status
csrrw_lazy (struct environ* env, csraddr_t addr, word send, word * recv, enum rwmod rw, struct req_stat * async_stat) {
    return env_csrrw_async (env, addr, send, recv, rw, async_stat);
}

status
csrrw_sync (struct environ* env, csraddr_t addr, word send, word * recv, enum rwmod rw) {
    return env_csrrw_sync (env, addr, send, recv, rw);
}

status
module_add_item (struct module * mod, enum module_item_type type, void * func) {
    status stat = STAT_SUCCESS;
    if (mod->items.item_n == MAX_MOD_ITEM) {
        stat |= ERR_NRITEM;
        goto error;
    }
    mod->items.item_list[mod->items.item_n].type = type;
    mod->items.item_list[mod->items.item_n].context = mod->context;
    mod->items.item_list[mod->items.item_n].func.ptr = func;
    mod->items.item_list[mod->items.item_n].modid = mod->modid;
    mod->items.item_list[mod->items.item_n].base = mod;
    mod->items.item_list[mod->items.item_n].magic = BISCUIT_MAGIC;
    mod->items.item_n++;
    return stat;
    error:
    return stat;
}

status
env_ctrl_mod (struct environ* env, modid_t modid, enum mod_ctl_arg arg, uint64 exarg) {
    return env_mod_ctrl (env, modid, arg, exarg);
}

status
env_add_mod (struct environ * env, struct module * mod) {
    return env_add_module (env, mod);
}

status
env_remove_mod (struct environ * env, modid_t modid) {
    return env_remove_module (env, modid);
}


