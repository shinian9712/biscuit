#include <stdlib.h>
#include "include/params.h"
#include "include/types.h"
#include "include/operat.h"
#include "include/module.h"
#include "include/virtware.h"
#include "include/libcore.h"
#include "include/mymods.h"
#include "include/vmerr.h"

status
modlist_add (struct modlist * list, struct module * mod) {
    status stat;
    stat = STAT_SUCCESS;
    if (list->mod_n < MAX_MODS) {
        list->list[list->mod_n++] = mod;
    } else {
        stat |= ERR_NRMOD;
        goto error;
    }
    return stat;
    error:
    return stat;
}

//在这里添加mod
status
modlist_init (struct modlist * list) {
    status stat = STAT_SUCCESS;
    list->mod_n = 0;
    struct module * rv32g = (struct module *) malloc (sizeof (struct module));
    struct module * bv32vm = (struct module *) malloc (sizeof (struct module));
    struct module * chrdev = (struct module *) malloc (sizeof (struct module));
    struct module * magicfs = (struct module *) malloc (sizeof (struct module));
    stat |= rv32g_assemble (rv32g);
    stat |= bv32vm_assemble (bv32vm);
    stat |= chrdev_assemble (chrdev);
    stat |= mfs_assemble (magicfs);
    modlist_add (list, rv32g);
    modlist_add (list, bv32vm);
    modlist_add (list, chrdev);
    modlist_add (list, magicfs);
    return stat;
}

status
modlist_install_all (struct environ * env, struct modlist * modlist) {
    status stat = STAT_SUCCESS;
    count_t i;
    for (i = 0; i < modlist->mod_n; i++) {
        stat |= env_add_module (env, modlist->list[i]);
    }
    return stat;
}

status
raise_excep_sync (struct environ* env, word cause) {
    return cpu_raise_excep_sync (&env->cpu, cause);
}

status
raise_excep_async (struct environ* env, word cause) {
    return devctl_raise_excep_async (&env->dctl, cause);
}
